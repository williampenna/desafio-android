package com.example.williampenna.gitrepos.Model.Pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by williampenna on 14/12/17.
 */

public class PullRequest {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;
    @SerializedName("body")
    private String description;
    @SerializedName("user")
    private AuthorPojo author;

    public PullRequest(int id, String title, String description, String url, AuthorPojo author) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.url = url;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public AuthorPojo getAuthor() {
        return author;
    }

    public void setAuthor(AuthorPojo author) {
        this.author = author;
    }
}
