package com.example.williampenna.gitrepos.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by williampenna on 13/12/17.
 */

public class CustomTextView extends AppCompatTextView {

    public CustomTextView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = CustomFontCache.getTypeface("fonts/Raleway-Regular.ttf", context);
        setTypeface(customFont);
    }

}
