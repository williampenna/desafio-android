package com.example.williampenna.gitrepos.Presenters;

import android.util.Log;

import com.example.williampenna.gitrepos.Interfaces.GithubInterface;
import com.example.williampenna.gitrepos.Model.Pojo.ReposPojo;
import com.example.williampenna.gitrepos.Model.Services.GithubService;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by williampenna on 12/12/17.
 */

public class GithubPresenter implements GithubInterface.ProvidedPresenterOps {

    private WeakReference<GithubInterface.RequiredViewOps> mView;
    private GithubInterface.ProvidedModelOps service;

    public GithubPresenter(GithubInterface.RequiredViewOps view) {
        mView = new WeakReference<>(view);
        service = new GithubService(mView.get().getActivityContext(), this);
    }

    @Override
    public void getRepos() {
        service.getRepos();
    }

    @Override
    public void onSuccessGetRepos(final List<ReposPojo> repos) {
        try {
            mView.get().getActivityContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mView.get().onSuccessGetRepos(repos);
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorGetRepos() {
        Log.e("Get repos", "Error");
    }
}
