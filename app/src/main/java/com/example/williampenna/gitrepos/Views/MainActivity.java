package com.example.williampenna.gitrepos.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.williampenna.gitrepos.Interfaces.GithubInterface;
import com.example.williampenna.gitrepos.Model.Adapters.ReposAdapter;
import com.example.williampenna.gitrepos.Model.Pojo.ReposPojo;
import com.example.williampenna.gitrepos.Presenters.GithubPresenter;
import com.example.williampenna.gitrepos.R;
import com.example.williampenna.gitrepos.Utils.EndlessRecyclerOnScrollListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import org.buraktamturk.loadingview.LoadingView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GithubInterface.RequiredViewOps, ReposAdapter.RepoCallback {

    private GithubInterface.ProvidedPresenterOps presenterOps;
    private LoadingView loadingView;
    private SwipyRefreshLayout swipyRefreshLayout;
    private RecyclerView recyclerView;
    private List<ReposPojo> reposPojos;
    private ReposAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(getResources().getString(R.string.most_popular));

        loadingView = (LoadingView) findViewById(R.id.loading_view);
        swipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipy_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.rview_repos);
        presenterOps = new GithubPresenter(this);

        linearLayoutManager = new LinearLayoutManager(this);

        reposPojos = new ArrayList<>();
        loadingView.setLoading(true);
        presenterOps.getRepos();

    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                downLoad();
            }
        });
    }

    @Override
    public MainActivity getActivityContext() {
        return this;
    }

    @Override
    public void onSuccessGetRepos(List<ReposPojo> repos) {
//        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivityContext(), 1);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        if (reposPojos.size() == 0) {
            reposPojos = repos;
            loadingView.setLoading(false);
            adapter = new ReposAdapter(getActivityContext(), reposPojos);
            recyclerView.setAdapter(adapter);
            adapter.setCallBack(getActivityContext());
        } else {
            reposPojos = repos;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorGetRepos() {
        loadingView.setLoading(false);
    }

    @Override
    public void repoSelected(String creator, String repository) {
        Intent intent = new Intent(this, PullRequestsActivity.class);
        intent.putExtra(getResources().getString(R.string.creator), creator);
        intent.putExtra(getResources().getString(R.string.repository_name), repository);
        startActivity(intent);
    }

    public void downLoad(){
        presenterOps.getRepos();
    }

}
