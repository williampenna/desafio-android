package com.example.williampenna.gitrepos.Model.Services;

import android.content.Context;
import android.util.Log;

import com.example.williampenna.gitrepos.Interfaces.PullRequestsInterface;
import com.example.williampenna.gitrepos.Model.Pojo.AuthorPojo;
import com.example.williampenna.gitrepos.Model.Pojo.PullRequest;
import com.example.williampenna.gitrepos.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by williampenna on 14/12/17.
 */

public class PullRequestsService implements PullRequestsInterface.ProvidedModelOps {

    private Context context;
    private PullRequestsInterface.ProvidedPresenterOps presenterOps;
    OkHttpClient client;
    private ArrayList<PullRequest> pullRequests;
    private Gson gson;

    public PullRequestsService(Context context, PullRequestsInterface.ProvidedPresenterOps presenterOps) {
        this.context = context;
        this.presenterOps = presenterOps;
        pullRequests = new ArrayList<>();
    }

    @Override
    public void getPullRequests(final String creator, final String repo) {
        String url = context.getResources().getString(R.string.github_api) + context.getResources().getString(R.string.repos) + creator + "/" + repo + context.getResources().getString(R.string.pulls);
        client = new OkHttpClient.Builder()
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .build();
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .get()
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Repos", call.toString());
                presenterOps.onErrorGetPullRequests();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                try {
                    JSONArray jsonArray = new JSONArray(responseString);
                    for (int i=0; i<jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("id");
                        String url = jsonObject.getString("url");
                        String title = jsonObject.getString("title");
                        String description = jsonObject.getString("body");
                        JSONObject authorJson = jsonObject.getJSONObject("user");
                        String username = authorJson.getString("login");
                        String photoUrl = authorJson.getString("avatar_url");
                        AuthorPojo authorPojo = new AuthorPojo(username, photoUrl);
                        PullRequest pull = new PullRequest(id, title, description, url, authorPojo);
                        pullRequests.add(pull);
                    }
                    presenterOps.onSuccessGetPullRequests(pullRequests);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
