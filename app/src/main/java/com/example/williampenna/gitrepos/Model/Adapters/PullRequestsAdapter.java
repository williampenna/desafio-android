package com.example.williampenna.gitrepos.Model.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.williampenna.gitrepos.Model.Pojo.AuthorPojo;
import com.example.williampenna.gitrepos.Model.Pojo.PullRequest;
import com.example.williampenna.gitrepos.R;

import java.util.ArrayList;

/**
 * Created by williampenna on 14/12/17.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PullRequest> pullRequests;
    private PullRequestsCallback pullRequestsCallback;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView username, fullName, repositoryName, repositoryDescription;
        public ImageView authorPhoto;
        public String url;

        public ViewHolder(View view){
            super(view);
            authorPhoto = view.findViewById(R.id.author_photo);
            username = view.findViewById(R.id.username);
            fullName = view.findViewById(R.id.full_name);
            repositoryName = view.findViewById(R.id.repository_name);
            repositoryDescription = view.findViewById(R.id.repository_description);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pullRequestsCallback.pullRequestSelected(url);
                }
            });
        }

    }

    public PullRequestsAdapter(Context context, ArrayList<PullRequest> pullRequests) {
        this.context = context;
        this.pullRequests = pullRequests;
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_request_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pull = pullRequests.get(position);
        AuthorPojo author = pull.getAuthor();
        Glide.with(context).load(author.getAuthorPhotoUrl()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.authorPhoto);
        holder.username.setText(author.getAuthorName());
        holder.fullName.setText("");
        holder.repositoryName.setText(pull.getTitle());
        holder.repositoryDescription.setText(pull.getDescription());
        holder.url = pull.getUrl();
    }

    public void setCallBack(PullRequestsCallback callback) {
        this.pullRequestsCallback = callback;
    }

    public interface PullRequestsCallback {
        void pullRequestSelected(final String url);
    }

}
