package com.example.williampenna.gitrepos.Model.Services;

import android.content.Context;
import android.util.Log;

import com.example.williampenna.gitrepos.Interfaces.GithubInterface;
import com.example.williampenna.gitrepos.Model.Pojo.AuthorPojo;
import com.example.williampenna.gitrepos.Model.Pojo.ReposPojo;
import com.example.williampenna.gitrepos.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOError;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by williampenna on 11/12/17.
 */

public class GithubService implements GithubInterface.ProvidedModelOps {

    private Context context;
    GithubInterface.ProvidedPresenterOps presenterOps;
    OkHttpClient client;
    private ArrayList<ReposPojo> reposPojos = new ArrayList<>();
    private int page;
    private Gson gson;

    public GithubService(Context context, GithubInterface.ProvidedPresenterOps presenterOps) {
        this.context = context;
        this.presenterOps = presenterOps;
        gson = new Gson();
        page = 1;
    }

    @Override
    public void getRepos() {
        String u = context.getResources().getString(R.string.github_api)+context.getResources().getString(R.string.search_repos);//?q=language:Java&sort=stars&page=1";
        String url = HttpUrl.parse(u).newBuilder()
                .addQueryParameter("q", "language:Java")
                .addQueryParameter("sort", "stars")
                .addQueryParameter("page", String.valueOf(page))
                .build().toString();
        client = new OkHttpClient.Builder()
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .build();
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .get()
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Repos", call.toString());
                presenterOps.onErrorGetRepos();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    JSONArray jsonArray = jsonObject.getJSONArray("items");
                    for (int i=0; i<jsonArray.length(); i++) {
                        JSONObject repoJson = jsonArray.getJSONObject(i);
                        ReposPojo repo = gson.fromJson(repoJson.toString(), ReposPojo.class);
                        reposPojos.add(repo);
                    }
                    page++;
                    presenterOps.onSuccessGetRepos(reposPojos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
