package com.example.williampenna.gitrepos.Views;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.williampenna.gitrepos.Interfaces.PullRequestsInterface;
import com.example.williampenna.gitrepos.Model.Adapters.PullRequestsAdapter;
import com.example.williampenna.gitrepos.Model.Pojo.PullRequest;
import com.example.williampenna.gitrepos.Presenters.PullRequestsPresenter;
import com.example.williampenna.gitrepos.R;

import org.buraktamturk.loadingview.LoadingView;

import java.util.ArrayList;

/**
 * Created by williampenna on 14/12/17.
 */

public class PullRequestsActivity extends AppCompatActivity implements PullRequestsInterface.RequiredViewOps, PullRequestsAdapter.PullRequestsCallback {

    private PullRequestsInterface.ProvidedPresenterOps presenterOps;
    private LoadingView loadingView;
    private RecyclerView recyclerView;
    private ArrayList<PullRequest> pullRequests;
    private PullRequestsAdapter adapter;
    private String creator;
    private String repositoryName;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pull_requests_activity);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        this.creator = getIntent().getStringExtra(getResources().getString(R.string.creator));
        this.repositoryName =  getIntent().getStringExtra(getResources().getString(R.string.repository_name));
        setTitle(repositoryName);

        loadingView = (LoadingView) findViewById(R.id.loading_view);
        recyclerView = (RecyclerView) findViewById(R.id.rv_pull_requests);
        pullRequests = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(this);

        presenterOps = new PullRequestsPresenter(this);
        loadingView.setLoading(true);
        presenterOps.getPullRequests(this.creator, this.repositoryName);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public PullRequestsActivity getActivityContext() {
        return this;
    }

    @Override
    public void onSuccessGetPullRequests(ArrayList<PullRequest> pulls) {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        pullRequests = pulls;
        loadingView.setLoading(false);
        adapter = new PullRequestsAdapter(getActivityContext(), pullRequests);
        recyclerView.setAdapter(adapter);
        adapter.setCallBack(getActivityContext());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorGetPullRequests() {

    }

    @Override
    public void pullRequestSelected(final String url) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(getResources().getString(R.string.url), url);
        startActivity(intent);
    }
}
