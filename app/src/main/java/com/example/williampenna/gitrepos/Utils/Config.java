package com.example.williampenna.gitrepos.Utils;

/**
 * Created by williampenna on 11/12/17.
 */

public class Config {

    private String url = "https://api.github.com/";

    public Config() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
