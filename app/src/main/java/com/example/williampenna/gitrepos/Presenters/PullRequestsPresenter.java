package com.example.williampenna.gitrepos.Presenters;

import com.example.williampenna.gitrepos.Interfaces.PullRequestsInterface;
import com.example.williampenna.gitrepos.Model.Pojo.PullRequest;
import com.example.williampenna.gitrepos.Model.Services.PullRequestsService;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by williampenna on 14/12/17.
 */

public class PullRequestsPresenter implements PullRequestsInterface.ProvidedPresenterOps {

    private WeakReference<PullRequestsInterface.RequiredViewOps> mView;
    private PullRequestsInterface.ProvidedModelOps service;

    public PullRequestsPresenter(PullRequestsInterface.RequiredViewOps view) {
        mView = new WeakReference<>(view);
        service = new PullRequestsService(mView.get().getActivityContext(), this);
    }

    @Override
    public void getPullRequests(final String creator, final String repo) {
        service.getPullRequests(creator, repo);
    }

    @Override
    public void onSuccessGetPullRequests(final ArrayList<PullRequest> pulls) {
        try {
            mView.get().getActivityContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mView.get().onSuccessGetPullRequests(pulls);
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorGetPullRequests() {

    }

}
