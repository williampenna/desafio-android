package com.example.williampenna.gitrepos.Model.Pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by williampenna on 13/12/17.
 */

public class AuthorPojo {

    @SerializedName("login")
    private String authorName;
    @SerializedName("avatar_url")
    private String authorPhotoUrl;

    public AuthorPojo(String authorName, String authorPhotoUrl) {
        this.authorName = authorName;
        this.authorPhotoUrl = authorPhotoUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorPhotoUrl() {
        return authorPhotoUrl;
    }

    public void setAuthorPhotoUrl(String authorPhotoUrl) {
        this.authorPhotoUrl = authorPhotoUrl;
    }
}
