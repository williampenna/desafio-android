package com.example.williampenna.gitrepos.Model.Pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by williampenna on 13/12/17.
 */

public class ReposPojo {

    @SerializedName("name")
    private String repoName;
    @SerializedName("description")
    private String repoDescription;
    @SerializedName("owner")
    private AuthorPojo author;
    @SerializedName("stargazers_count")
    private int starsNumber;
    @SerializedName("forks_count")
    private int forksNumber;

    public ReposPojo(String repoName, String repoDescription, AuthorPojo author, int starsNumber, int forksNumber) {
        this.repoName = repoName;
        this.repoDescription = repoDescription;
        this.author = author;
        this.starsNumber = starsNumber;
        this.forksNumber = forksNumber;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

    public void setRepoDescription(String repoDescription) {
        this.repoDescription = repoDescription;
    }

    public AuthorPojo getAuthor() {
        return author;
    }

    public void setAuthor(AuthorPojo author) {
        this.author = author;
    }

    public int getStarsNumber() {
        return starsNumber;
    }

    public void setStarsNumber(int starsNumber) {
        this.starsNumber = starsNumber;
    }

    public int getForksNumber() {
        return forksNumber;
    }

    public void setForksNumber(int forksNumber) {
        this.forksNumber = forksNumber;
    }
}
