package com.example.williampenna.gitrepos.Interfaces;

import com.example.williampenna.gitrepos.Model.Pojo.PullRequest;
import com.example.williampenna.gitrepos.Views.PullRequestsActivity;

import java.util.ArrayList;

/**
 * Created by williampenna on 14/12/17.
 */

public interface PullRequestsInterface {

    interface RequiredViewOps {
        PullRequestsActivity getActivityContext();
        void onSuccessGetPullRequests(ArrayList<PullRequest> pulls);
        void onErrorGetPullRequests();
    }

    interface ProvidedPresenterOps {
        void getPullRequests(final String creator, final String repo);
        void onSuccessGetPullRequests(ArrayList<PullRequest> pulls);
        void onErrorGetPullRequests();
    }

    interface ProvidedModelOps {
        void getPullRequests(final String creator, final String repo);
    }

}
