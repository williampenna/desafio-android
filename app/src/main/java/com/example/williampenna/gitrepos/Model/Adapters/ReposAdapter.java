package com.example.williampenna.gitrepos.Model.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.williampenna.gitrepos.Model.Pojo.AuthorPojo;
import com.example.williampenna.gitrepos.Model.Pojo.ReposPojo;
import com.example.williampenna.gitrepos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by williampenna on 13/12/17.
 */

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ViewHolder> {

    private Context context;
    private List<ReposPojo> reposPojos;
    private RepoCallback repoCallback;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView username, fullName, forksNumber, starsNumber, repositoryName, repositoryDescription;
        public ImageView authorPhoto;

        public ViewHolder(View view){
            super(view);
            authorPhoto = view.findViewById(R.id.author_photo);
            username = view.findViewById(R.id.username);
            fullName = view.findViewById(R.id.full_name);
            forksNumber = view.findViewById(R.id.forks_number);
            starsNumber = view.findViewById(R.id.stars_number);
            repositoryName = view.findViewById(R.id.repository_name);
            repositoryDescription = view.findViewById(R.id.repository_description);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    repoCallback.repoSelected(username.getText().toString(), repositoryName.getText().toString());
                }
            });
        }

    }

    public ReposAdapter(Context context, List<ReposPojo> reposPojos) {
        this.context = context;
        this.reposPojos = reposPojos;
    }

    @Override
    public int getItemCount() {
        return reposPojos.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.repos_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ReposPojo reposPojo = reposPojos.get(position);
        AuthorPojo author = reposPojo.getAuthor();
        Glide.with(context).load(author.getAuthorPhotoUrl()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.authorPhoto);
        holder.username.setText(author.getAuthorName());
        holder.fullName.setText("");
        holder.forksNumber.setText(reposPojo.getForksNumber() + " " + context.getResources().getString(R.string.forks));
        holder.starsNumber.setText(reposPojo.getStarsNumber() + " " + context.getResources().getString(R.string.stars));
        holder.repositoryName.setText(reposPojo.getRepoName());
        holder.repositoryDescription.setText(reposPojo.getRepoDescription());
    }

    public void setCallBack(RepoCallback repoCalback){
        this.repoCallback = repoCalback;
    }

    public interface RepoCallback {
        void repoSelected(final String creator, final String repository);
    }

}
