package com.example.williampenna.gitrepos.Interfaces;

import com.example.williampenna.gitrepos.Views.MainActivity;
import com.example.williampenna.gitrepos.Model.Pojo.ReposPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by williampenna on 11/12/17.
 */

public interface GithubInterface {

    interface RequiredViewOps {
        MainActivity getActivityContext();
        void onSuccessGetRepos(List<ReposPojo> repos);
        void onErrorGetRepos();
    }

    interface ProvidedPresenterOps {
        void getRepos();
        void onSuccessGetRepos(List<ReposPojo> repos);
        void onErrorGetRepos();
    }

    interface ProvidedModelOps {
        void getRepos();
    }

}
